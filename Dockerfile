
FROM debian:bullseye-slim

ENV GITLAB_DEPS="git"
ENV GODOT_DEPS="scons pkgconf gcc g++"
ENV PLATFORM_LINUXBSD_DEPS="xorg-dev"
ENV PLATFORM_OSX_DEPS=""
ENV PLATFORM_WIN_DEPS=""

RUN apt-get update && apt-get install -y $GITLAB_DEPS $GODOT_DEPS $PLATFORM_LINUXBSD_DEPS && apt-get clean
